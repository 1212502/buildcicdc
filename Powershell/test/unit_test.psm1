$scriptDir = $PSScriptRoot;
Import-Module "$scriptDir\filesystem.psm1" -Force -DisableNameChecking

function Invoke-ComponentTestApplication 
{
	param (
		[string] [Parameter(Mandatory=$true)]$workspacePath
	 )

	$testOutputLocation = "$workspacePath\TestResults"

	Write-MainStepMessage -message "Running tests"
	Run-UnitestsV3  -workspacePath $workspacePath -outputLocation $testOutputLocation -modulePath $modulePath
}

function Write-MainStepMessage($message = $(throw "-message is required."))
{
    $messageSeperator = Create-Seperator -character "*" -length 150

    Write-Host $messageSeperator
    Write-Host $message
    Write-Host $messageSeperator
}

function Create-Seperator($character = $(throw "-character is required."), $length = 150)
{
    if ($length -gt 150)
    {
        $length = 150
    }

    $seperator = $character * $length;

    return ,$seperator;
}

function Run-UnitestsV3($workspacePath, $outputLocation , $modulePath)
{
	$nunitconsole = Install-Nunit
	write-host "`tGetting testassemblies from $workspacePath"
    $unitTestAssemblies = $(Get-ChildItem $workspacePath -include "*.Test.dll" -Exclude "*Integration.*.dll" -Recurse) | foreach { if ( $_.FullName -notmatch "(.*)\\obj\\(.*)" ) { $_.FullName }  }
    $unitTestAssemblies += $(Get-ChildItem $workspacePath -include "*.Tests.dll" -Exclude "*Integration.*.dll" -Recurse) | foreach { if ( $_.FullName -notmatch "(.*)\\obj\\(.*)" ) { $_.FullName }  }
    $unitTestAssemblies += $(Get-ChildItem $workspacePath -include "*.Specs.dll" -Exclude "*Integration.*.dll" -Recurse) | foreach { if ( $_.FullName -notmatch "(.*)\\obj\\(.*)" ) { $_.FullName }  }
    write-host "`tFound testassemblies:"
    $unitTestAssemblies | ForEach-Object { Write-Host "`t$_" }

    $unitestingFailed = $false

    if (Test-Path $outputLocation)
    {
        Remove-Item "$outputLocation" -Force -Confirm:$False -Recurse
    }
    
    Create-Directory "$outputLocation"
    write-host "`r`n`tStart testing"
    foreach ($testAssembly in $unitTestAssemblies)
    {
        $fileName = [System.IO.Path]::GetFileNameWithoutExtension($testAssembly)
        write-host "`tTesting $fileName"
        
        & $nunitconsole --labels=OnOutputOnly --result=$outputLocation\NUnit.$fileName.xml`;format=nunit2 $testAssembly
        
        if ($LASTEXITCODE -ne 0)
        {
            $unitestingFailed = $true;
            write-host "`t$testAssembly - NUnit failed with exit code $LASTEXITCODE"   
        }
        else         
        {
            write-host "`t$testAssembly - Success"
        }        
    }
    
    if ($unitestingFailed)
    {
        Throw-Failure -error "`t Nunit failed"
    }
    elseif ($unitTestAssemblies.Count -lt 1)
    {
        Throw-Failure -errorMessage "`t Geen testbestanden gevonden" -error "Nunit failed"
    }
    else
    {
         Write-Host "`t Alle testen geslaagd"
    }
}

function Install-Nunit()
{
    Install-NuGetPackage -package "NUnit.Extension.NUnitV2ResultWriter" -version "3.6.0" | out-null
	Install-NuGetPackage -package "NUnit.Extension.NUnitV2Driver" -version "3.8.0" | out-null
	$nunitLocation = Install-NuGetPackage -package "NUnit.ConsoleRunner" -version "3.11.1"
	$nunitconsole = [System.IO.Path]::Combine($nunitLocation, "tools\nunit3-console.exe")
	return ,$nunitconsole
}

function Install-NuGetPackage($package = $(throw "package is required."), $version = $(throw "version is required."))
{
    $packageInstallLocation = "$PSScriptRoot\packages";
    $supportToolLocation = "$PSScriptRoot\.nuget";
	$nuGet = "$supportToolLocation\nuget.exe"
	$arguments = "install {0} -OutputDirectory ""{1}"" -NoCache -ExcludeVersion -version {2}" -f $package, $packageInstallLocation, $version
	
	Write-Host "`t Installing $package version $version with $nuGet";
	
	Invoke-BatchCommand -program $nuGet -argumentString $arguments
	
	$installLocation = [System.IO.Path]::Combine($packageInstallLocation, $package)
	return ,$installLocation
}

function Invoke-BatchCommand($program = $(throw "-program is required."), $argumentString)
{
	Write-Host "Invoking BatchCommand '$program' with parameters '$argumentString'"
	
	if (!([System.IO.File]::Exists($program)))
	{
		try
		{
			& $program
		}
		catch
		{
			throw "`t`tError: Could not find program $program"
		}
	}
	
    $psi = new-object "Diagnostics.ProcessStartInfo"
    $fileName = [System.IO.Path]::GetFileName($program)
    $workingDirectory = [System.IO.Path]::GetDirectoryName($program)

    $psi.FileName = $program
    $psi.WorkingDirectory  = $workingDirectory
    $psi.Arguments = $argumentString
    $psi.RedirectStandardOutput = $true;
    $psi.RedirectStandardError = $true;
    $psi.UseShellExecute = $false;
    $psi.Verb = "runas";

    $proc = [Diagnostics.Process]::Start($psi)
    $proc.WaitForExit();

    if ($proc.ExitCode -ne "0")
    {
        $error = $proc.StandardError.ReadToEnd()
        $output = $proc.StandardOutput.ReadToEnd()
        Write-Host "Error when executing $program with arguments $argumentString"
        if($error -ne $null){
            Write-Host "Error message is: $error"
        }
        if($output -ne $null){
            Write-Host "Output message is: $output"
        }
        throw "Error executing $fileName. Error: $error"
    }
}
function Throw-Failure($errorMessage, $error)
{
    Write-Host "`r`rError/failed"
    Create-Seperator -character "-"
    Write-Host "$errorMessage"
    Create-Seperator -character "-"
    throw $error 
}