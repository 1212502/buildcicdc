function Remove-Directory($directory = $(throw "-directory is required."), $retryCount = 0)
{
    try
    {
        if([System.IO.Directory]::Exists("$directory"))
		{
			Remove-Item $directory -Recurse -Force -Confirm:$false | Out-Null
								
			if([System.IO.Directory]::Exists("$directory"))
			{
				throw "`t`tERROR: Directory $directory is not removed!"
			}
		}
    }
    catch
    {
        $retryCount++;
        if ($retryCount -lt 5)
        {
			Write-Host "`tWARNING: could not remove directory $directory. Retrying attempt $retryCount/5"
			Start-Sleep -s 5
            Remove-Directory -directory $directory -retryCount $retryCount
        }
		else
		{
			throw $_.Exception
		}
    }
}

function Create-Directory($directory = $(throw "-directory is required."))
{
    if (!([System.IO.Directory]::Exists("$directory")))
	{
        New-Item -path "$directory" -type directory | Out-Null
        if([System.IO.Directory]::Exists("$directory"))
        {
			Give-Everyone-Rights -directory "$directory";
        } 
		else 
		{
            throw "`t`tERROR: Directory $directory is not created!"
        }
    } 
}

function Recreate-Directory($directory = $(throw "-directory is required."))
{
    If ([System.IO.Directory]::Exists($directory))
    {
        Get-ChildItem -Path $directory -Recurse | Remove-Item -force -recurse
    }
	else
	{
		New-Item -ItemType directory -Path $directory | Out-Null
		Give-Everyone-Rights -directory $directory;
	}
}

function Create-Folders($destination, $folders)
{
    foreach ($folder in $folders)
    {
        $path = "{0}\{1}" -f $destination,$folder  
        if (-not (Test-Path $path))
        {
            Write-Host "`t`tCreating folder $path"
            New-Item -ItemType Directory -Path $path | Out-Null
        }
        else 
        {
            Write-Host "`t`tFolder $path already exists. Skipping creation"
        }
    }
}

function Give-Everyone-Rights($directory = $(throw "-directory is required."))
{
	$InheritanceFlag = [System.Security.AccessControl.InheritanceFlags]"ContainerInherit, ObjectInherit" 
	$PropagationFlag = [System.Security.AccessControl.PropagationFlags]::None 

	$objType =[System.Security.AccessControl.AccessControlType]::Allow 

	# "S-1-1-0" is the SID of the User Account 'Everyone', but it works on non-english Windows as well because Id over localized strings.
	$objUser = New-Object System.Security.Principal.SecurityIdentifier("S-1-1-0")
	$colRights = [System.Security.AccessControl.FileSystemRights]"FullControl" 
	$objACE = New-Object System.Security.AccessControl.FileSystemAccessRule($objUser, $colRights, $InheritanceFlag, $PropagationFlag, $objType) 
	
	$objACL = Get-ACL $directory
	$objACL.AddAccessRule($objACE) 
	Set-ACL $directory $objACL
}

function Copy-FilesToFileShare ($path, $fileshareCredential, $source, $destination) 
{
    Write-Host "Delete shares to $path"	
    net use $path /delete /y > $null 2>&1

    Write-Host "Create share for path $([string]$path)"	
    $drive = New-PSDrive -PSProvider FileSystem -Root $path -Credential $fileshareCredential -Name "$([guid]::NewGuid())" -ErrorAction Stop
        
    try 
    {
        $destination = "Microsoft.PowerShell.Core\FileSystem::$($drive.Root)\$destination"
        Write-Host "Copying files from $source to destination $destination"
        Copy-Item -Path $source -Destination $destination -Recurse -Force 
    }
    catch 
    {
        throw $_
    }
    finally 
    {
        Remove-PSDrive -Name $drive.Name -Force | Out-Null
        Write-Host "Remove share for path $([string]$drive.Root)"	    
    }
}

function Copy-FS-Files($source = $(throw "-source is required."), $destination = $(throw "-destination is required."), $fileName, $ExcludeEmptyDirectories = $False)
{
    if ($source.EndsWith("\"))
    {
        $source = $source.Remove($source.Length - 1, 1);
    }
            
    if ($destination.EndsWith("\"))
    {
        $destination = $destination.Remove($destination.Length - 1, 1);
    }

    if ([string]::IsNullOrEmpty($fileName))
    {
		if (!([System.IO.Directory]::Exists("$source")))
		{
			throw "`t`tERROR: '$source' does not exists"
		}
        
        $ExcludeEmptyDirectoriesParameter = "/e"
        
        If ($ExcludeEmptyDirectories) {
            $ExcludeEmptyDirectoriesParameter = "/s"
        }
        
        Write-Host "Copy-FS-Files $([string]$source) to $([string]$destination)"
        Robocopy "$source" "$destination" /copy:dat $ExcludeEmptyDirectoriesParameter /R:5 | Out-Null
    }
    else
    {
		if (![System.IO.File]::Exists("$source\$fileName") -and !($fileName.Contains("*")))
		{
			throw "`t`tERROR: '$source\$fileName' does not exists"
		}
        
        Write-Host "Copy-FS-Files $([string]$source)to $([string]$destination) (only file? $([string]$fileName))"
        Robocopy "$source" "$destination" $fileName /xd $destination /copy:dat /s /R:5 | Out-Null #/s zorgt ervoor dat er geen empty folders meegekopieerd worden.
    }
}

function Copy-File($source = $(throw "-source is required."), $destination = $(throw "-destination is required."))
{
	# Zie https://sqlsanctum.wordpress.com/2015/07/09/powershell-remote-copy-workaround/ voor uitleg over de 'FileSystem::'
	if(!(Test-Path -path "FileSystem::$destination"))
	{
		New-Item "FileSystem::$destination" -Type Directory | Out-Null
	}
	
    Copy-Item "FileSystem::$source" "FileSystem::$destination" -Force | Out-Null
}

function Move-Files($source = $(throw "-source is required."), $destination = $(throw "-destination is required."), $fileName)
{
    if ($source.EndsWith("\"))
    {
        $source = $source.Remove($source.Length - 1, 1);
    }
            
    if ($destination.EndsWith("\"))
    {
        $destination = $destination.Remove($destination.Length - 1, 1);
    }

    if ([string]::IsNullOrEmpty($fileName))
    {
		if (!([System.IO.Directory]::Exists("$source")))
		{
			throw "`t`tERROR: '$source' does not exists"
		}
		
        Robocopy "$source" "$destination" /MOVE /e /R:5 | Out-Null
    }
    else
    {
		if (![System.IO.File]::Exists("$source\$fileName"))
		{
			throw "`t`tERROR: '$source\$fileName' does not exists"
		}
        Robocopy "$source" "$destination" /MOVE $fileName | Out-Null 
    }
}

function Rename-FS-File($from = $(throw "-source is required."), $to = $(throw "-destination is required."))
{
	if (!([System.IO.File]::Exists("$from")))
	{
		throw "`t`tERROR: '$from' does not exists"
	}
	
	Move-Item "$from" "$to" -force
}

function Download-FileFromNetwork($from = $(throw "-from is required."), $to = $(throw "-to is required."), $username = $(throw "-username is required."), $password = $(throw "-password is required."))
{	
	if ($to.EndsWith("\"))
    {
        $to = $to.Remove($to.Length - 1, 1);
    }
            
    if ($from.EndsWith("\"))
    {
        $from = $from.Remove($from.Length - 1, 1);
    }

	$fromPathRoot = [System.IO.Path]::GetPathRoot($from)
	$filePattern = [System.IO.Path]::GetFileName($from)
	$containingDirectory = [System.IO.Path]::GetDirectoryName($from)
	
	try
	{
		net use $fromPathRoot $password /user:$username > $null 2>&1 #password en username voor de share inladen

		$fileSearchResult = @(Get-ChildItem $containingDirectory -include "$filePattern" -recurse);
		if ($fileSearchResult.Length -eq 0)
		{
			throw "`t`tERROR: Filesearch '$filePattern' did not return any results on '$from' ($containingDirectory)"
		}

		$fileName = $fileSearchResult[0].Name;
		$containingDirectory = [System.IO.Path]::GetDirectoryName($fileSearchResult) #mogelijk zit de file nog een aantal mappen dieper 
		if(!(Test-Path $to))
        {
            New-Item -ItemType Directory -Force -Path $to
        }
        Robocopy "$containingDirectory" "$to" $fileName /copy:dat /R:5 | Out-Null 

	}
	finally
	{
		net use $fromPathRoot /delete  > $null 2>&1
	}
}

function Clear-Directory($directory = $(throw "-directory is required."), $exclude = "")
{ 
    if([System.IO.Directory]::Exists("$directory"))
    {
        Get-ChildItem $directory | Where { $_ -notlike $exclude } | Remove-Item -Recurse -Force
    } 
    else 
    {
        throw "`t`tERROR: Directory $directory does not exist!"
    }
}

function Shorten-Directory($directory)
{
    $folders = @(Get-ChildItem $directory | ?{ $_.PSIsContainer } )

    for($i=0; $i -lt $folders.Count; $i++)
    {
        $folder = $folders[$i].FullName;
        
        $newFolderName = Get-FolderName -directory $directory -index $i

        Rename-Item -Path $folder -NewName $newFolderName
        
        Shorten-Directory -directory "$directory\$newFolderName"
    }
}

function Get-FolderName($directory, $index)
{
    if (Test-Path "$directory\$index")
    {
        $index = $index + 1;
        $index = Get-FolderName -directory $directory -index $index
    }

    return ,$index
}

function Get-RemoteFiles($source, $credentials, $filters)
{
    try {
        $psdrive = $null
        $psdriveArgs = @{ Name=([guid]::NewGuid()); PSProvider="FileSystem"; Root=$source; Credential=$credentials }
		
        $psdrive = New-PSDrive @psdriveArgs -ErrorAction Stop
        $files = Get-ChildItem -Path "$psdrive`:" -Include $filters -Recurse
        return $files
    }
    catch {
        Throw $_
    }
    finally {
        #  Remove the PSDrive to avoid the multiple connections error.
        Remove-PSDrive $psdrive | Out-Null
    }
}