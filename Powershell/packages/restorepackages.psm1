function Restore-NugetPackages($workspacePath)
{
	$nuget  = "$PSScriptRoot\.nuget\nuget.exe"
	$nugetConfig  = "$PSScriptRoot\.nuget\nuget.Config"
	$solutionFiles = Get-ChildItem $workspacePath -filter "*.sln"
	foreach ($solutionFile in $solutionFiles)
	{
		$solution = $solutionFile.FullName
		& $nuget Restore $solution -NoCache -ConfigFile $nugetConfig
		if ($LASTEXITCODE -ne 0)
		{
			throw "Restoring NuGet packages for solution $solution failed (NuGet exited with code $LASTEXITCODE)"   
		}
		&dotnet restore $solution --no-cache --configfile $nugetConfig
		if ($LASTEXITCODE -ne 0)
		{
			throw "Restoring NuGet packages for solution $solution failed (dotnet exited with code $LASTEXITCODE)"   
		}
	}
}